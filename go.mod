module space-images

go 1.13

require (
	github.com/gofiber/fiber/v2 v2.1.2
	github.com/klauspost/compress v1.11.0 // indirect
	github.com/sirupsen/logrus v1.7.0
	github.com/sqs/goreturns v0.0.0-20181028201513-538ac6014518
	github.com/stretchr/testify v1.6.1
	golang.org/x/lint v0.0.0-20201208152925-83fdc39ff7b5 // indirect
	golang.org/x/sys v0.0.0-20201029080932-201ba4db2418 // indirect
	golang.org/x/tools v0.0.0-20201218024724-ae774e9781d2 // indirect
	google.golang.org/genproto v0.0.0-20201119123407-9b1e624d6bc4 // indirect
)
