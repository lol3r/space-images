package nasa

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestHasContent(t *testing.T) {
	tables := []struct {
		Name        string
		InputBundle *imageBundle
		Result      bool
	}{
		{
			Name: "Has Content",
			InputBundle: &imageBundle{
				Images: map[int]*Image{
					0: {},
				},
			},
			Result: true,
		},
		{
			Name: "Has no Content",
			InputBundle: &imageBundle{
				Images: map[int]*Image{},
			},
			Result: false,
		},
	}

	for _, table := range tables {
		inBundle := table.InputBundle
		result := table.Result

		t.Run(table.Name, func(t *testing.T) {
			output := inBundle.HasContent()

			assert.Equal(t, result, output)
		})
	}
}
