package nasa

// HasContent returns whether or not this image bundle actually
// contains any images
func (bundle *imageBundle) HasContent() bool {
	return len(bundle.Images) > 0
}
