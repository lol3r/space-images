package nasa

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestExtractImageTitle(t *testing.T) {
	tables := []struct {
		Name         string
		InputBody    []byte
		ResultString string
		ResultError  bool
	}{
		{
			Name:         "Empty input body",
			InputBody:    []byte(""),
			ResultString: "",
			ResultError:  true,
		},
		{
			Name:         "Valid input 1",
			InputBody:    []byte(examplePage),
			ResultString: "Mars and Meteor over Jade Dragon Snow Mountain",
			ResultError:  false,
		},
		{
			Name:         "Valid input 2",
			InputBody:    []byte(examplePage1),
			ResultString: "Great Conjunction: Saturn and Jupiter Converge",
			ResultError:  false,
		},
		{
			Name:         "Valid input 3",
			InputBody:    []byte(examplePage2),
			ResultString: "Worlds of a Distant Sun: 47 Ursae Majoris b",
			ResultError:  false,
		},
		{
			Name:         "Valid input 4",
			InputBody:    []byte(examplePage3),
			ResultString: "Arp 230: Two Spirals in One?",
			ResultError:  false,
		},
		{
			Name:         "Valid input 5",
			InputBody:    []byte(examplePage4),
			ResultString: "Watch Galaxies Form",
			ResultError:  false,
		},
		{
			Name:         "Valid input 6",
			InputBody:    []byte(examplePage5),
			ResultString: "A Mysterious Cone Nebula",
			ResultError:  false,
		},
	}

	for _, table := range tables {
		inBody := table.InputBody
		resultString := table.ResultString
		resultError := table.ResultError

		t.Run(table.Name, func(t *testing.T) {
			outString, outError := extractImageTitle(inBody)

			if resultError {
				assert.NotNil(t, outError)
			} else {
				assert.Nil(t, outError)
			}
			assert.Equal(t, resultString, outString)
		})
	}
}
