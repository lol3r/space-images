package nasa

// ToKey returns the Key that corresponds to the API-Date
func (a *APIDate) ToKey() int {
	return a.Year*10000 + a.Month*100 + a.Day
}
