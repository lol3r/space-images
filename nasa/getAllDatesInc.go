package nasa

import (
	"time"
)

func getAllDatesInc(start APIDate, end APIDate) []APIDate {
  result :=  make([]APIDate, 0);

  t1 := time.Date(start.Year, time.Month(start.Month), start.Day, 12, 0, 0, 0, time.UTC)
  t2 := time.Date(end.Year, time.Month(end.Month), end.Day, 12, 0, 0, 0, time.UTC)

  days := int(t2.Sub(t1).Hours() / 24) + 1

  tmp := t1
  for i := 0; i < days; i++ {
    result = append(result, APIDate{
      Year: tmp.Year(),
      Month: int(tmp.Month()),
      Day: tmp.Day(),
    })

    tmp = tmp.Add(time.Hour * 24)
  }

  return result;
}
