package nasa

import "errors"

// GetRandomImage returns a random Image from the Image bundle
func (bundle *imageBundle) GetRandomImage() (*Image, error) {
	if !bundle.HasContent() {
		return nil, errors.New("No Images in bundle")
	}

	for {
		randomDate := GetRandomDate(bundle.Start, bundle.End)
		value, found := bundle.Images[randomDate.ToKey()]

		if found {
			return value, nil
		}
	}
}
