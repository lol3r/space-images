package nasa

import (
	"io/ioutil"
	"net/http"

	"github.com/sirupsen/logrus"
)

func loadImage(date APIDate) (*Image, error) {
  pageURL := "https://apod.nasa.gov/apod/ap" + date.getURLDateString() + ".html"

  resp, err := http.Get(pageURL)
  if err != nil {
    return nil, err
  }
  defer func() {
    if err := resp.Body.Close(); err != nil {
      logrus.Errorf("Closing Response-Body: %v", err)
    }
  }()

  body, err := ioutil.ReadAll(resp.Body)
  if err != nil {
    return nil, err
  }

  return parseImageBody(pageURL, date, body)
}
