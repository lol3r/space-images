package nasa

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestParseImageBody(t *testing.T) {
	tables := []struct {
		Name        string
		InputURL    string
		InputDate   APIDate
		InputBody   []byte
		ResultImage Image
		ResultError bool
	}{
		{
			Name:     "Valid Input 1",
			InputURL: "http://example.com/1",
			InputDate: APIDate{
				Day:   13,
				Month: 7,
				Year:  2015,
			},
			InputBody: []byte(examplePage),
			ResultImage: Image{
				Title:    "Mars and Meteor over Jade Dragon Snow Mountain",
				PageURL:  "http://example.com/1",
				ImageURL: "https://apod.nasa.gov/apod/image/2011/LeonidmeteorandMarsoverYulongsnowmountain1050.jpg",
				Date: APIDate{
					Day:   13,
					Month: 7,
					Year:  2015,
				},
			},
		},
	}

	for _, table := range tables {
		inURL := table.InputURL
		inDate := table.InputDate
		inBody := table.InputBody
		resImage := table.ResultImage
		resError := table.ResultError

		t.Run(table.Name, func(t *testing.T) {
			outImage, outError := parseImageBody(inURL, inDate, inBody)

			if resError {
				assert.NotNil(t, outError)
			} else {
				assert.Nil(t, outError)
			}

			assert.Equal(t, resImage, *outImage)
		})
	}
}

func BenchmarkParseImageBody(b *testing.B) {
	inURL := "http://example.com/input"
	inDate := APIDate{
		Day:   10,
		Month: 10,
		Year:  2010,
	}
	inBody := []byte(examplePage)

	for n := 0; n < b.N; n++ {
		_, err := parseImageBody(inURL, inDate, inBody)
		if err != nil {
			fmt.Printf("Error")
		}
	}
}
