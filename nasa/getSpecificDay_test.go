package nasa

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestGetSpecificImage(t *testing.T) {
	tables := []struct {
		Name        string
		InputBundle *imageBundle
		InputDay    APIDate
		ResultImage Image
		ResultError bool
	}{
		{
			Name: "Valid",
			InputBundle: &imageBundle{
				Images: map[int]*Image{
					20201009: &Image{
						Title:    "testImage",
						ImageURL: "http://example.com/image",
						PageURL:  "http://example.com/page",
						Date: APIDate{
							Year:  2020,
							Month: 10,
							Day:   9,
						},
					},
				},
			},
			InputDay: APIDate{
				Year:  2020,
				Month: 10,
				Day:   9,
			},
			ResultImage: Image{
				Title:    "testImage",
				ImageURL: "http://example.com/image",
				PageURL:  "http://example.com/page",
				Date: APIDate{
					Year:  2020,
					Month: 10,
					Day:   9,
				},
			},
			ResultError: false,
		},
		{
			Name: "Day not found",
			InputBundle: &imageBundle{
				Images: map[int]*Image{
					20201009: &Image{
						Title:    "testImage",
						ImageURL: "http://example.com/image",
						PageURL:  "http://example.com/page",
						Date: APIDate{
							Year:  2020,
							Month: 10,
							Day:   9,
						},
					},
				},
			},
			InputDay: APIDate{
				Year:  2010,
				Month: 10,
				Day:   9,
			},
			ResultImage: Image{},
			ResultError: true,
		},
	}

	for _, table := range tables {
		inBundle := table.InputBundle
		inDay := table.InputDay
		resImage := table.ResultImage
		resError := table.ResultError

		t.Run(table.Name, func(t *testing.T) {
			outImage, outError := inBundle.GetSpecificDay(inDay)

			if resError {
				assert.NotNil(t, outError)
			} else {
				assert.Nil(t, outError)
				assert.Equal(t, resImage, *outImage)
			}
		})
	}
}
