package nasa

// GetEmptyImageBundle returns an empty ImageBundle, for when you need to
// instantiate a struct where this will later be loaded with actual data
func GetEmptyImageBundle() ImageBundle {
	return &imageBundle{
		Images: map[int]*Image{},
		Start:  APIDate{0, 0, 0},
		End:    APIDate{0, 0, 0},
	}
}
