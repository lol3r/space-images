package nasa

import (
	"math/rand"
	"time"
)

// GetRandomDate returns a random Date between the Start and End date
func GetRandomDate(start, end APIDate) APIDate {
	startTime := time.Date(start.Year, time.Month(start.Month), start.Day, 0, 0, 0, 0, time.UTC)
	endTime := time.Date(end.Year, time.Month(end.Month), end.Day, 0, 0, 0, 0, time.UTC)

	startTimestamp := startTime.Unix()
	endTimestamp := endTime.Unix()

	randTimestamp := rand.Int63n(endTimestamp-startTimestamp) + startTimestamp
	randTime := time.Unix(randTimestamp, 0)

	return APIDate{
		Year:  randTime.Year(),
		Month: int(randTime.Month()),
		Day:   randTime.Day(),
	}
}
