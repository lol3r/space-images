package nasa

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestDateToKey(t *testing.T) {
	tables := []struct {
		Name      string
		InputDate APIDate
		ResultKey int
	}{
		{
			Name: "Valid",
			InputDate: APIDate{
				Year:  2020,
				Month: 12,
				Day:   20,
			},
			ResultKey: 20201220,
		},
	}

	for _, table := range tables {
		inDate := table.InputDate
		resKey := table.ResultKey

		t.Run(table.Name, func(t *testing.T) {
			outKey := inDate.ToKey()

			assert.Equal(t, resKey, outKey)
		})
	}
}
