package nasa

import (
	"errors"
	"regexp"
)

var imageRegex = regexp.MustCompile(`(?i)<img src="(.*?)"`)
var quotationRegex = regexp.MustCompile(`"`)

func extractImageURL(body []byte) (string, error) {
	matched := imageRegex.Find(body)
	if matched == nil {
		return "", errors.New("Could not find Image")
	}

	rawContent := quotationRegex.FindIndex(matched)
	if rawContent == nil {
		return "", errors.New("Could not extract Image path")
	}

	imagePath := matched[rawContent[0]+1 : len(matched)-1]

	imageURL := "https://apod.nasa.gov/apod/" + string(imagePath)

	return imageURL, nil
}
