package nasa

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestExtractImageURL(t *testing.T) {
	tables := []struct {
		Name         string
		InputBody    []byte
		ResultString string
		ResultError  bool
	}{
		{
			Name:         "Empty input body",
			InputBody:    []byte(""),
			ResultString: "",
			ResultError:  true,
		},
		{
			Name:         "Valid Input 1",
			InputBody:    []byte(examplePage),
			ResultString: "https://apod.nasa.gov/apod/image/2011/LeonidmeteorandMarsoverYulongsnowmountain1050.jpg",
			ResultError:  false,
		},
		{
			Name:         "Valid Input 2",
			InputBody:    []byte(examplePage1),
			ResultString: "https://apod.nasa.gov/apod/image/2012/SaturnJupiter_Voltmer_960.jpg",
			ResultError:  false,
		},
	}

	for _, table := range tables {
		inBody := table.InputBody
		resultString := table.ResultString
		resultError := table.ResultError

		t.Run(table.Name, func(t *testing.T) {
			outString, outError := extractImageURL(inBody)

			if resultError {
				assert.NotNil(t, outError)
			} else {
				assert.Nil(t, outError)
			}
			assert.Equal(t, resultString, outString)
		})
	}
}
