package nasa

func parseImageBody(pageURL string, date APIDate, body []byte) (*Image, error) {
  imageURL, err := extractImageURL(body)
  if err != nil {
    return nil, err
  }

  title, err := extractImageTitle(body)
  if err != nil {
    return nil, err
  }

  result := Image{
    Title: title,
    PageURL: pageURL,
    ImageURL: imageURL,
    Date: date,
  }
  return &result, nil
}
