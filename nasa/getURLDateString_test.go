package nasa

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestGetURLDateString(t *testing.T) {
  tables := []struct{
    Name string
    InputDate APIDate
    Result string
  }{
    {
      Name: "Valid Stuff",
      InputDate: APIDate{
        Year: 2020,
        Month: 11,
        Day: 2,
      },
      Result: "201102",
    },
  }

  for _, table := range tables {
    inDate := table.InputDate
    result := table.Result

    t.Run(table.Name, func(t *testing.T) {
      output := inDate.getURLDateString()

      assert.Equal(t, result, output)
    })
  }
}
