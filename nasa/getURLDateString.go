package nasa

import (
	"fmt"
	"strings"
)

func zeroPadToTwo(input int) string {
  number := input - ((input / 100) * 100)

  return fmt.Sprintf("%02d", number)
}

func (m *APIDate) getURLDateString() string {
  var builder strings.Builder

  builder.WriteString(zeroPadToTwo(m.Year))
  builder.WriteString(zeroPadToTwo(m.Month))
  builder.WriteString(zeroPadToTwo(m.Day))

  return builder.String()
}
