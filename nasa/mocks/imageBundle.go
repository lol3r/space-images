package mocks

import (
	"space-images/nasa"

	"github.com/stretchr/testify/mock"
)

// MockImageBundle is a simple mock implementation used for testing
type MockImageBundle struct {
	mock.Mock
}

// HasContent is needed to comply with the interface
func (m *MockImageBundle) HasContent() bool {
	args := m.Called()
	return args.Bool(0)
}

// GetRandomImage is needed to comply with the interface
func (m *MockImageBundle) GetRandomImage() (*nasa.Image, error) {
	args := m.Called()
	return args.Get(0).(*nasa.Image), args.Error(1)
}

// GetSpecificDay is needed to comply with the interface
func (m *MockImageBundle) GetSpecificDay(date nasa.APIDate) (*nasa.Image, error) {
	args := m.Called(date)
	return args.Get(0).(*nasa.Image), args.Error(1)
}
