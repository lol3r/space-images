package nasa

// This has been taken from https://apod.nasa.gov/apod/ap201121.html
const examplePage = `<!doctype html>
<html>
<head>
<title> APOD: 2020 November 21 - Mars and Meteor over Jade Dragon Snow Mountain
</title> 
<!-- gsfc meta tags -->
<meta name="orgcode" content="661">
<meta name="rno" content="phillip.a.newman">
<meta name="content-owner" content="Jerry.T.Bonnell.1">
<meta name="webmaster" content="Stephen.F.Fantasia.1">
<meta name="description" content="A different astronomy and space science
related image is featured each day, along with a brief explanation.">
<!-- -->
<meta name=viewport content="width=device-width, initial-scale=1">
<meta name=keywords content="mars, leonid, meteor">
<!-- -->
<script id="_fed_an_ua_tag"
src="//dap.digitalgov.gov/Universal-Federated-Analytics-Min.js?agency=NASA">
</script>


</head>

<body BGCOLOR="#F4F4FF" text="#000000" link="#0000FF" vlink="#7F0F9F"
alink="#FF0000">

<center>
<h1> Astronomy Picture of the Day </h1>
<p>

<a href="archivepix.html">Discover the cosmos!</a>
Each day a different image or photograph of our fascinating universe is
featured, along with a brief explanation written by a professional astronomer.
<p>

2020 November 21
<br> 
<a href="image/2011/LeonidmeteorandMarsoverYulongsnowmountain.jpg">
<IMG SRC="image/2011/LeonidmeteorandMarsoverYulongsnowmountain1050.jpg"
alt="See Explanation.  Clicking on the picture will download
the highest resolution version available." style="max-width:100%"></a>

</center>

<center>
<b> Mars and Meteor over Jade Dragon Snow Mountain </b> <br>

<b> Image Credit &
<a href="lib/about_apod.html#srapply">Copyright</a>:</b>

<a href="http://www.twanight.org/Dai">Jeff Dai</a>
(<a href="http://www.twanight.org/">TWAN</a>)

</center> <p> 

<b> Explanation: </b> 

A brilliant yellowish celestial beacon, Mars still
<a href="https://earthsky.org/astronomy-essentials/visible-planets-tonight-mars-jupiter-venus-saturn-mercury">dazzles</a> in the night.

<a href="ap201001.html">Peering</a> between clouds the wandering planet was
briefly joined by the flash of a meteor in this moonless
dark sky on November 18.

The single exposure was taken as the Earth swept up dust from
periodic comet Tempel-Tuttle during the annual
<a href="https://solarsystem.nasa.gov/asteroids-comets-and-meteors/meteors-and-meteorites/leonids/in-depth/">Leonid Meteor Shower</a>.

The view of a rugged western horizon looks along
the Yulong mountain range in Yunnan province, southwestern China.

Yulong
(<a href="https://en.wikipedia.org/wiki/Jade_Dragon_Snow_Mountain">Jade Dragon</a>)
Snow Mountain lies below the clouds and
beyond the end of the meteor streak.

<p> <center>

<b> Tomorrow's picture: </b><a href="ap201122.html">dark marking on the sky</a>

<p> <hr>
<a href="ap201120.html">&lt;</a>
| <a href="archivepix.html">Archive</a>
| <a href="lib/apsubmit2015.html">Submissions</a> 
| <a href="lib/aptree.html">Index</a>
| <a href="https://antwrp.gsfc.nasa.gov/cgi-bin/apod/apod_search">Search</a>
| <a href="calendar/allyears.html">Calendar</a>
| <a href="/apod.rss">RSS</a>
| <a href="lib/edlinks.html">Education</a>
| <a href="lib/about_apod.html">About APOD</a>
| <a href=
"http://asterisk.apod.com/discuss_apod.php?date=201121">Discuss</a>
| <a href="ap201122.html">&gt;</a>

<hr><p>
<b> Authors & editors: </b>
<a href="http://www.phy.mtu.edu/faculty/Nemiroff.html">Robert Nemiroff</a>
(<a href="http://www.phy.mtu.edu/">MTU</a>) &
<a href="https://antwrp.gsfc.nasa.gov/htmltest/jbonnell/www/bonnell.html"
>Jerry Bonnell</a> (<a href="http://www.astro.umd.edu/">UMCP</a>)<br>
<b>NASA Official: </b> Phillip Newman
<a href="lib/about_apod.html#srapply">Specific rights apply</a>.<br>
<a href="https://www.nasa.gov/about/highlights/HP_Privacy.html">NASA Web
Privacy Policy and Important Notices</a><br>
<b>A service of:</b>
<a href="https://astrophysics.gsfc.nasa.gov/">ASD</a> at
<a href="https://www.nasa.gov/">NASA</a> /
<a href="https://www.nasa.gov/centers/goddard/">GSFC</a>
<br><b>&</b> <a href="http://www.mtu.edu/">Michigan Tech. U.</a><br>
</center>
</body>
</html>
`

// Found at https://apod.nasa.gov/apod/ap201215.html
const examplePage1 = `<!doctype html>
<html>
<head>
<title> APOD: 2020 December 15 - Great Conjunction: Saturn and Jupiter Converge                  
</title> 
<!-- gsfc meta tags -->
<meta name="orgcode" content="661">
<meta name="rno" content="phillip.a.newman">
<meta name="content-owner" content="Jerry.T.Bonnell.1">
<meta name="webmaster" content="Stephen.F.Fantasia.1">
<meta name="description" content="A different astronomy and space science
related image is featured each day, along with a brief explanation.">
<!-- -->
<meta name=viewport content="width=device-width, initial-scale=1">
<meta name="keywords" content="Saturn, Jupiter, great conjunction">
<!-- -->
<script id="_fed_an_ua_tag"
src="//dap.digitalgov.gov/Universal-Federated-Analytics-Min.js?agency=NASA">
</script>

</head>

<body BGCOLOR="#F4F4FF" text="#000000" link="#0000FF" vlink="#7F0F9F"
alink="#FF0000">

<center>
<h1> Astronomy Picture of the Day </h1>
<p>

<a href="archivepix.html">Discover the cosmos!</a>
Each day a different image or photograph of our fascinating universe is
featured, along with a brief explanation written by a professional astronomer.
<p>

2020 December 15  
<br> 
 <a href="image/2012/SaturnJupiter_Voltmer_1920.jpg">
<IMG SRC="image/2012/SaturnJupiter_Voltmer_960.jpg"
alt="See Explanation.  Clicking on the picture will download
the highest resolution version available." style="max-width:100%"></a>
</center>

<center>
<b> Great Conjunction: Saturn and Jupiter Converge </b> <br> 
<b> Illustration Credit & Copyright: </b> 
<a href="https://apollo-13.eu/en/team/">Sebastian Voltmer</a> 
</center> <p> 

<b> Explanation: </b> 
It's happening.

<a href="https://solarsystem.nasa.gov/planets/saturn/overview/"
>Saturn</a> and 
<a href="https://solarsystem.nasa.gov/planets/jupiter/overview/">Jupiter</a> 
are moving closer and will soon appear in almost exactly the same direction. 

Coincidentally, on the night of the 
<a href="https://en.wikipedia.org/wiki/December_solstice">December solstice</a> 
-- the longest night of the year in the north and the longest day in the south -- the long-awaited 
<a href="https://en.wikipedia.org/wiki/Great_conjunction">Great Conjunction</a> will occur.

Then, about six days from now, 
<a href="https://www.adlerplanetarium.org/blog/jupiter-and-saturn-great-conjunction/"
>Saturn and Jupiter</a> will be right next to each other -- 
as they are every 20 years.

But this juxtaposition is not just any 
<a href="https://earthsky.org/astronomy-essentials/great-jupiter-saturn-conjunction-dec-21-2020"
>Great Conjunction</a> -- it will be the closest since 
<a href="https://en.wikipedia.org/wiki/1623">1623</a> 
because the two planetary giants will pass only 1/10th of a 
<a href="http://www.1728.org/angsize.htm">degree</a> from each other -- 
well less than the apparent diameter of a full moon. 

In the next few days a 
<a href="ap201208.html">crescent moon</a> will also pass a few degrees away from the 
<a href="https://www.forbes.com/sites/jamiecartereurope/2020/11/17/when-and-where-you-can-see-a-stunning-triangle-of-lights-as-jupiter-and-saturn-meet-the-moon-this-week/"
>converging planets</a> 
and give a preliminary 
<a href="https://solarsystem.nasa.gov/news/1615/how-to-photograph-the-conjunction-of-saturn-and-jupiter/">opportunity for iconic photos</a>. 

The featured illustration shows the approach of 
<a href="ap201212.html">Saturn and Jupiter</a> 
during November and December over the French 
<a href="https://youtu.be/XAo0B4klQWM">Alps</a>.


<p> <center> 
<b> Growing Gallery: </b> 
<a href="https://www.facebook.com/media/set/?vanity=APOD.Sky&set=a.3211133262324204"
>Notable images of the Great Conjunction submitted to APOD</a> <br>
<b> Tomorrow's picture: </b><a href="ap201216.html">sounding out dark matter</a>

<p> <hr>
<a href="ap201214.html">&lt;</a>
| <a href="archivepix.html">Archive</a>
| <a href="lib/apsubmit2015.html">Submissions</a> 
| <a href="lib/aptree.html">Index</a>
| <a href="https://antwrp.gsfc.nasa.gov/cgi-bin/apod/apod_search">Search</a>
| <a href="calendar/allyears.html">Calendar</a>
| <a href="/apod.rss">RSS</a>
| <a href="lib/edlinks.html">Education</a>
| <a href="lib/about_apod.html">About APOD</a>
| <a href=
"http://asterisk.apod.com/discuss_apod.php?date=201215">Discuss</a>
| <a href="ap201216.html">&gt;</a>

<hr><p>
<b> Authors & editors: </b>
<a href="http://www.phy.mtu.edu/faculty/Nemiroff.html">Robert Nemiroff</a>
(<a href="http://www.phy.mtu.edu/">MTU</a>) &
<a href="https://antwrp.gsfc.nasa.gov/htmltest/jbonnell/www/bonnell.html"
>Jerry Bonnell</a> (<a href="http://www.astro.umd.edu/">UMCP</a>)<br>
<b>NASA Official: </b> Phillip Newman
<a href="lib/about_apod.html#srapply">Specific rights apply</a>.<br>
<a href="https://www.nasa.gov/about/highlights/HP_Privacy.html">NASA Web
Privacy Policy and Important Notices</a><br>
<b>A service of:</b>
<a href="https://astrophysics.gsfc.nasa.gov/">ASD</a> at
<a href="https://www.nasa.gov/">NASA</a> /
<a href="https://www.nasa.gov/centers/goddard/">GSFC</a>
<br><b>&</b> <a href="http://www.mtu.edu/">Michigan Tech. U.</a><br>
</center>
</body>
</html>`

// From https://apod.nasa.gov/apod/ap960701.html
const examplePage2 = `<head>
<title> APOD: July 1, 1996 - Worlds of a Distant Sun: 47 Ursae Majoris b
</title>
</head>
<body bgcolor="#F4F4FF" text="#000000"  link="#0000FF"
                       vlink="#7F0F9F" alink="#FF0000">

<center>
<h1> Astronomy Picture of the Day </h1>
<p>

<a href="archivepix.html">Discover the cosmos!</a>
Each day a different image or photograph of our fascinating universe is
featured, along with a brief explanation written by a professional
astronomer. <p>

July 1, 1996
<br>
<a href="image/whatm_47uma1_big.jpg">
<IMG SRC="image/whatm_47uma1.gif"
alt="See Explanation.  Clicking on the picture will download 
 the highest resolution version available."></a>

<br>

<b> Worlds of a Distant Sun: 47 Ursae Majoris b</b> <br>
<b> Credit and Copyright: </b> 
John Whatmough,
<a href="http://www.empire.net/~whatmoug/Extrasolar/extrasolar_visions.html"
>Extrasolar Visions</a>

</center>
<p>

<b> Explanation: </b> 
Within the last few years, observational astronomy has given humanity
<a href="http://www.obspm.fr:80/departement/darc/planets/encycl.html"
>evidence of the existence of worlds</a> beyond the
solar system.  Indeed, solar-type stars are now inferred to
<a href="ap960131.html">harbor planets of approximately
Jupiter mass</a> - some residing in temperature zones
which could conceivably
<a href="ap951118.html">support liquid water</a> and
<a href="http://bang.lanl.gov/solarsys/edu/lifeuniv.htm">therefore life</a>!
Above is a hypothetical scene near one such planet
whose sun, 47 Ursae Majoris (47 UMa), is a yellow dwarf star
<a href="http://gpu.srv.ualberta.ca/~tfoster/turtorial.htm"
>(spectral class G0 V)</a> very similar to our own.  In our sky, it
appears as a faint, inconspicuous
star below the cup of the
<a href="http://pio06.urel.berkeley.edu/documentation/gifs/dipper.jpg"
>northern hemisphere asterism, "the Big Dipper"</a>.
(Our own sun would be equally inconspicuous
<a href="http://www.seti-inst.edu/">when viewed from</a> 47 UMa ...)
<a href="http://cannon.sfsu.edu/~williams/planetsearch/planetsearch.html"
>Astronomer's G. Marcy and P. Butler</a> announced the
<a href="http://cannon.sfsu.edu/~williams/planetsearch/47UMa/47UMa.html"
>discovery of a planet associated with this star</a>
in 1996 and reported it to have a mass of
about 2.4 Jupiters or more with an orbital period of 3 years.
<a href="http://www.empire.net/~whatmoug/Extrasolar/home_47UMa.html"
>This artist's vision</a> pictures the detected planet,
referred to as 47 UMa b, as a gas giant surrounded by a ring of
material - analogous to our own
<a href="http://antwrp.gsfc.nasa.gov/apod/lib/saturn.html"
>gas giant Saturn</a>.
In the foreground lies a hypothetical moon of 47 UMa b.  
<a href="ap950905.html">Could such a moon support life?</a>
47 UMa is only 44 light years distant, fairly
<a href="http://www.astro.wisc.edu/~dolan/constellations/extra/nearest.html"
>close by astronomical standards</a> - yet there is evidence for
<a href="http://gag.observ-gr.fr:80/~listserv/exoplanets/0000.html"
>planetary systems which are closer</a> still.
<a href ="http://techinfo.jpl.nasa.gov/WWW/ExNPS/HomePage.html"
>NASA plans to explore nearby planetary systems</a> using spaceborne
observatories.


<p> <center> 
<b> APOD News: </b> 
<a href="lib/rjn2mtu.html">RJN to MTU</a> <br>
<b> Tomorrow's picture: </b> NASA's Latest Rockets: X-33
<p> <hr> 

| <a href="archivepix.html">Archive</a> 
| <a href="lib/aptree.html">Index</a>
| <a href="/cgi-bin/cossc/apod_search">Search</a>
| <a href="lib/glossary.html">Glossary</a> 
| <a href="lib/edlinks.html">Education</a> 
| <a href="lib/about_apod.html">About APOD</a> | 

<hr>
<a 
href="http://xp3.dejanews.com/getdoc.xp?recnum=4949249&server=dnserver.db96q1&CONTEXT=834947392.21423&hitnum=3">
<IMG SRC="image/coolyear.gif"
alt="See Explanation.  Clicking on the picture will download 
 the highest resolution version available."></a>

<a href="http://magellan.mckinley.com/rr_bd.cgi?52013"> 
<IMG SRC="image/star4_mag.gif"
alt="See Explanation.  Clicking on the picture will download 
 the highest resolution version available."></a>

<a href="http://www.pointcom.com/gifs/reviews/zzsts001.htm"> 
<img alt="Top 5 logo" src="image/top5percent.gif"></a>

<a href="http://www-e1c.gnn.com/gnn/wic/wics/sci.new.html">
<IMG SRC="image/wic_select.gif"
alt="See Explanation.  Clicking on the picture will download 
 the highest resolution version available."></a>
<hr>

<p>
<b> Authors & editors: </b>
<a href="http://antwrp.gsfc.nasa.gov/htmltest/rjn.html">Robert Nemiroff</a>
(<a href="http://www.phy.mtu.edu/">MTU</a>) &
<a 
href="http://antwrp.gsfc.nasa.gov/htmltest/jbonnell/www/bonnell.html">Jerry
Bonnell</a> (<a href="http://www.usra.edu/">USRA</a>).<br>
<b>NASA Technical Rep.: </b>
<a href="http://heasarc.gsfc.nasa.gov/docs/bios/sherri.html">Sherri 
Calvo</a>.
Specific rights apply.<br>
<b>A service of:</b>
<a href="http://lheawww.gsfc.nasa.gov/docs/lhea/homepg_lhea.html">LHEA</a>
at
<a href="http://www.gsfc.nasa.gov/NASA_homepage.html">NASA</a>/
<a href="http://www.gsfc.nasa.gov/GSFC_homepage.html">GSFC</a><br>
</center>
</body>`

// From https://apod.nasa.gov/apod/ap960822.html
const examplePage3 = `<head>
<title> APOD: August 22, 1996 - Arp 230: Two Spirals in One?
</title>
</head>
<body bgcolor="#F4F4FF" text="#000000"  link="#0000FF"
                       vlink="#7F0F9F" alink="#FF0000">

<center>
<h1> Astronomy Picture of the Day </h1>
<p>

<a href="archivepix.html">Discover the cosmos!</a>
Each day a different image or photograph of our fascinating universe is
featured, along with a brief explanation written by a professional
astronomer. <p>

August 22, 1996
<br>
<a href="image/arp230_hst_big.jpg">
<IMG SRC="image/arp230_hst.gif"
alt="See Explanation.  Clicking on the picture will download 
 the highest resolution version available."></a>
<br>

<b> Arp 230: Two Spirals in One? </b> <br>
<b> Credit:</b>
J. A. Westphal 
(<a href="http://astro.caltech.edu/">Caltech</a>),
<a href="http://scivax.stsci.edu/~hamilton/nuggets/HST_NUGGETS.HTML">
HST</a>,
<a href="http://www.stsci.edu/public.html">STSci</a>, 
<a href="http://www.nasa.gov/">NASA</a>
</center>
<p>

<b> Explanation: </b> 
Is this one galaxy or two?  Analysis of <a href=
"http://adsabs.harvard.edu/cgi-bin/nph-article_query?bibcode=1990AJ%2E%2E%2E%2E100%2E1073M&page=1&plate_select=NO&type=GIF"
>Arp 230</a> has shown evidence that this seemingly single
<a href="lib/spirals.html">spiral galaxy</a> is actually the result of the 
recent 
<a href="http://sdsc.edu/IOTW/week47/iotw.html">collision
of two spiral galaxies</a>.  The 
<a href="ap951228.html">slow motion collision</a> took place over about 100
million years and induced a burst of 
<a href="ap951107.html">star formation</a> that has begun to
subside. The collision apparently had many similarities to the colliding
galaxy sequence in the IMAX movie 
"<a href="http://zeus.ncsa.uiuc.edu:8080/Summers/scicomp.html">Cosmic 
Voyage</a>." 

<p> <center> 
<b> Tomorrow's picture: </b> NGC 3293: A Bright Young Open Cluster
<p> <hr> 

| <a href="archivepix.html">Archive</a> 
| <a href="lib/aptree.html">Index</a>
| <a href="/cgi-bin/cossc/apod_search">Search</a>
| <a href="lib/glossary.html">Glossary</a> 
| <a href="lib/edlinks.html">Education</a> 
| <a href="lib/about_apod.html">About APOD</a> | 

<hr>
<!a 
href="http://xp3.dejanews.com/getdoc.xp?recnum=4949249&server=dnserver.db96q1&CONTEXT=834947392.21423&hitnum=3">
<!IMG SRC="image/coolyear.gif"></a>

<!a href="http://magellan.mckinley.com/rr_bd.cgi?52013"> 
<!IMG SRC="image/star4_mag.gif"></a>

<!a href="http://www.pointcom.com/gifs/reviews/zzsts001.htm"> 
<!IMG SRC="image/top5percent.gif"></a>

<!a href="http://webcrawler.com/select/sci.astro.html">
<!IMG SRC="image/WCSelect.gif"></a>
<!hr>

<p>
<b> Authors & editors: </b>
<a href="http://antwrp.gsfc.nasa.gov/htmltest/rjn.html">Robert Nemiroff</a>
(<a href="http://www.phy.mtu.edu/">MTU</a>) &
<a 
href="http://antwrp.gsfc.nasa.gov/htmltest/jbonnell/www/bonnell.html">Jerry
Bonnell</a> (<a href="http://www.usra.edu/">USRA</a>).<br>
<b>NASA Technical Rep.: </b>
<a href="http://heasarc.gsfc.nasa.gov/docs/bios/sherri.html">Sherri 
Calvo</a>.
Specific rights apply.<br>
<b>A service of:</b>
<a href="http://lheawww.gsfc.nasa.gov/docs/lhea/homepg_lhea.html">LHEA</a>
at
<a href="http://www.gsfc.nasa.gov/NASA_homepage.html">NASA</a>/
<a href="http://www.gsfc.nasa.gov/GSFC_homepage.html">GSFC</a><br>
</center>
</body>`

// From https://apod.nasa.gov/apod/ap960905.html
const examplePage4 = `<head>
<title> APOD: September 5, 1996 - Watch Galaxies Form 
</title>
</head>
<body bgcolor="#F4F4FF" text="#000000"  link="#0000FF"
                       vlink="#7F0F9F" alink="#FF0000">

<center>
<h1> Astronomy Picture of the Day </h1>
<p>

<a href="archivepix.html">Discover the cosmos!</a>
Each day a different image or photograph of our fascinating universe is
featured, along with a brief explanation written by a professional
astronomer. <p>

September 5, 1996
<br>
<a href="image/cluster_bk_big.jpg">
<IMG SRC="image/cluster_bk.gif"
alt="See Explanation.  Clicking on the picture will download 
 the highest resolution version available."></a>
<br>

<b> Watch Galaxies Form </b> <br>
<b> Credit:</b> 
S. Pascarell, R. Windhorst and S. Odewahn 
(<a href="http://www.asu.edu/clas/dopa/dopa.html"
>Arizona State U.</a>),
W. Keel (<a href="http://www.as.ua.edu/physics/">U. Alabama</a>), 
<a href="http://www.stsci.edu/">HST</a> 
</center>
<p>

<b> Explanation: </b> 
Snips and snails and puppy dog tails, is that what galaxies were 
made of?  In a 
<a href="http://www.stsci.edu/pubinfo/PR/96/29.html">report</a> 
released yesterday and soon to be published in 
<a href="http://www.nature.com/">Nature</a>, 
astronomers have imaged an interesting 
<a href="ap960628.html">distant patch of sky</a> 
with the orbiting 
<a href="ap950810.html">Hubble Space Telescope</a>.
They found many merging groups of 
<a href="lib/glossary.html#star">stars</a> and 
<a href="lib/glossary.html#hydrogen">gas</a> which have been 
dubbed "pre-galactic blobs."  A particularly dense bunch of 
<a href=" http://www.stsci.edu/pubinfo/PR/96/29/KeyColor.jpg"
>these small blue merging objects</a>
are visible in the above picture.  This may be a snapshot of 
<a href=" http://www.stsci.edu/pubinfo/mpeg/galaxies.mpg"
>galaxies actually being formed</a>!  
Although peculiar by present standards of 
<a href="ap960714.html">galaxies</a>,
these blobs may have been normal in the distant past, many billions of 
years ago. This adds evidence that 
<a href="http://141.142.3.134/Cyberia/Cosmos/CookingCosmos.html">galaxies 
formed</a> from the 
conglomeration of smaller objects instead of the 
<a href="http://adsabs.harvard.edu/cgi-bin/nph-bib_query?1995ApJ%2E%2E%2E450%2E%2E%2E%2E1A&db_key=AST"
>fragmentation of larger objects</a>. 

<p> <center> 
<b> Tomorrow's picture: </b> The Largest Impact Crater 
<p> <hr> 

| <a href="archivepix.html">Archive</a> 
| <a href="lib/aptree.html">Index</a>
| <a href="/cgi-bin/cossc/apod_search">Search</a>
| <a href="lib/glossary.html">Glossary</a> 
| <a href="lib/edlinks.html">Education</a> 
| <a href="lib/about_apod.html">About APOD</a> | 

<hr>
<!a 
href="http://xp3.dejanews.com/getdoc.xp?recnum=4949249&server=dnserver.db96q1&CONTEXT=834947392.21423&hitnum=3">
<!IMG SRC="image/coolyear.gif"></a>

<!a href="http://magellan.mckinley.com/rr_bd.cgi?52013"> 
<!IMG SRC="image/star4_mag.gif"></a>

<!a href="http://www.pointcom.com/gifs/reviews/zzsts001.htm"> 
<!IMG SRC="image/top5percent.gif"></a>

<!a href="http://webcrawler.com/select/sci.astro.html">
<!IMG SRC="image/WCSelect.gif"></a>
<!hr>

<p>
<b> Authors & editors: </b>
<a href="http://antwrp.gsfc.nasa.gov/htmltest/rjn.html">Robert Nemiroff</a>
(<a href="http://www.phy.mtu.edu/">MTU</a>) &
<a 
href="http://antwrp.gsfc.nasa.gov/htmltest/jbonnell/www/bonnell.html">Jerry
Bonnell</a> (<a href="http://www.usra.edu/">USRA</a>).<br>
<b>NASA Technical Rep.: </b>
<a href="http://heasarc.gsfc.nasa.gov/docs/bios/sherri.html">Sherri 
Calvo</a>.
Specific rights apply.<br>
<b>A service of:</b>
<a href="http://lheawww.gsfc.nasa.gov/docs/lhea/homepg_lhea.html">LHEA</a>
at
<a href="http://www.gsfc.nasa.gov/NASA_homepage.html">NASA</a>/
<a href="http://www.gsfc.nasa.gov/GSFC_homepage.html">GSFC</a><br>
</center>
</body>`

// FROM https://apod.nasa.gov/apod/ap960301.html
const examplePage5 = `<head>
<title> APOD: March 1, 1996 - A Mysterious Cone Nebula 
</title>
</head>
<body bgcolor="#F4F4FF">

<center>
<h1> Astronomy Picture of the Day </h1>
<p>

<a href="archivepix.html">Discover the cosmos!</a>
Each day a different image or photograph of our fascinating universe is
featured, along with a brief explanation written by a professional
astronomer. <p> 

March 1, 1996
<br>
<a href="image/cone_aat.gif">
<IMG SRC="image/cone_aat.gif"
alt="See Explanation.  Clicking on the picture will download 
 the highest resolution version available."></a>

<br>
<b> A Mysterious Cone Nebula </b> <br>
<b> Credit: </b> 
<a href="http://www.aao.gov.au/">Anglo-Australian Telescope</a> 
photograph by David Malin <br>
<b> Copyright: </b> 
<a href="http://www.aao.gov.au/images.html">Anglo-Australian Telescope
Board</a>
</center>
<p>

<b> Explanation: </b> 
Sometimes the simplest shapes are the hardest to explain.  For example, the
origin of the mysterious cone-shaped region located just below the center
of the above picture remains a mystery.  The dark region clearly contains
much 
<a href="lib/glossary.html#dust">dust</a> which blocks light from the 
<a href="ap950925.html">emission nebula</a> and 
<a href="ap960116.html">open cluster</a> NGC
2264 behind it.  
<a href="http://adsabs.harvard.edu/cgi-bin/nph-bib_query?1985cgd%2E%2Econf%2E%2E213B&db_key=AST"
>One hypothesis</a> holds that the 
<a href="http://www.stwing.upenn.edu/~jparker/astronomy/nebulae/ngc2264.html"
>cone</a> is formed by 
<a href="http://www.sti.nasa.gov/thesaurus/S/word14917.html">wind</a> 
particles from an energetic source blowing past
the <a href="ap951006.html">Bok globule</a> at the head of the cone. 

<p> <center> 
<b> Information:</b> 
<a href="http://antwrp.gsfc.nasa.gov/diamond_jubilee/debate_1996.html">The 
Scale of the Universe Debate in April 1996</a><br>
<b> Tomorrow's picture: </b>Von Braun's Wheel
<p> <hr> 

| <a href="archivepix.html">Archive</a> 
| <a href="lib/aptree.html">Index</a>
| <a href="/cgi-bin/cossc/apod_search">Search</a>
| <a href="lib/glossary.html">Glossary</a> 
| <a href="lib/edlinks.html">Education</a> 
| <a href="lib/about_apod.html">About APOD</a> | 

<hr>
<a href="http://www.jsc.nasa.gov/~mccoy/nasa/Cool.html">
<IMG SRC="image/coolyear.gif"
alt="See Explanation.  Clicking on the picture will download 
 the highest resolution version available."></a>

<a href="http://www.mckinley.com/"> <IMG SRC="image/star4_mag.gif"
alt="See Explanation.  Clicking on the picture will download 
 the highest resolution version available."></a>

<a href="http://www.pointcom.com/"> <img alt="Top 5 logo" src="image/top5percent.gif"></a>

<a href="http://www-e1c.gnn.com/gnn/wic/wics/sci.new.html">
<IMG SRC="image/wic_select.gif"
alt="See Explanation.  Clicking on the picture will download 
 the highest resolution version available."></a>
<hr>

<p>
<b> Authors & editors: </b>
<a href="http://antwrp.gsfc.nasa.gov/htmltest/rjn.html">Robert Nemiroff</a>
(<a href="http://www.gmu.edu/">GMU</a>) &
<a 
href="http://antwrp.gsfc.nasa.gov/htmltest/jbonnell/www/bonnell.html">Jerry
Bonnell</a> (<a href="http://www.usra.edu/">USRA</a>).<br>
<b>NASA Technical Rep.: </b>
<a href="http://heasarc.gsfc.nasa.gov/docs/bios/sherri.html">Sherri 
Calvo</a>.
Specific rights apply.<br>
<b>A service of:</b>
<a href="http://lheawww.gsfc.nasa.gov/docs/lhea/homepg_lhea.html">LHEA</a>
at
<a href="http://www.gsfc.nasa.gov/NASA_homepage.html">NASA</a>/
<a href="http://www.gsfc.nasa.gov/GSFC_homepage.html">GSFC</a><br>
</center>
</body>`
