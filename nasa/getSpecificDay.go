package nasa

import "errors"

func (bundle *imageBundle) GetSpecificDay(date APIDate) (*Image, error) {
	value, found := bundle.Images[date.ToKey()]
	if !found {
		return nil, errors.New("Could not find Image")
	}

	return value, nil
}
