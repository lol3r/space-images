package nasa

// APIDate is used to handle the Dates for the APOD archive in an easier way
type APIDate struct {
	Year  int `json:"year" query:"year"`
	Month int `json:"month" query:"month"`
	Day   int `json:"day" query:"day"`
}

// Image represents one Image from the APOD archive
type Image struct {
	Title    string  `json:"title"`
	ImageURL string  `json:"imageURL"`
	PageURL  string  `json:"pageURL"`
	Date     APIDate `json:"date"`
}

// ImageBundle represents a collection of loaded images
type ImageBundle interface {
	HasContent() bool
	GetRandomImage() (*Image, error)
	GetSpecificDay(date APIDate) (*Image, error)
}

type imageBundle struct {
	Images map[int]*Image
	Start  APIDate
	End    APIDate
}
