package nasa

import "github.com/sirupsen/logrus"

func min(a, b int) int {
	if a <= b {
		return a
	}
	return b
}

type batchResponse struct {
	Result *Image
	Date   APIDate
	Error  error
}

func handleBatch(dates []APIDate, resultChan chan batchResponse, doneChan chan bool) {
	for _, date := range dates {
		image, err := loadImage(date)

		resultChan <- batchResponse{
			Result: image,
			Date:   date,
			Error:  err,
		}
	}

	doneChan <- true
}

// LoadImages is used to load all the Images between the start and end dates
func LoadImages(start, end APIDate, loaderThreads int) ImageBundle {
	result := make(map[int]*Image, 0)

	dates := getAllDatesInc(start, end)

	dateCount := len(dates)
	chunkSize := dateCount / loaderThreads

	recvChannel := make(chan batchResponse)
	doneChan := make(chan bool)
	running := 0
	for i := 0; i < dateCount; i += chunkSize {
		batch := dates[i:min(i+chunkSize, dateCount)]
		go handleBatch(batch, recvChannel, doneChan)
		running++
	}

	logrus.Infof("Loading Images with %d Routines", running)

	for running > 0 {
		select {
		case received := <-recvChannel:
			if received.Error != nil {
				logrus.Errorf("Could not load image for %v: %v", received.Date, received.Error)
				continue
			}

			result[received.Date.ToKey()] = received.Result
		case <-doneChan:
			running--
		}
	}

	return &imageBundle{
		Images: result,
		Start:  start,
		End:    end,
	}
}
