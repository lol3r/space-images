package nasa

import (
	"errors"
	"regexp"
)

var titleRegex = regexp.MustCompile(`(?i)(<center>|</a>.*\n.*(\n)?<br>(\n)?\n)(\n)*<b>.*<\/b>`)
var innerSelectorRegex = regexp.MustCompile(`>.*</`)

func extractImageTitle(body []byte) (string, error) {
	matched := titleRegex.Find(body)
	if matched == nil {
		return "", errors.New("Could not find title")
	}

	rawContent := innerSelectorRegex.FindIndex(matched)
	if rawContent == nil {
		return "", errors.New("Could not extract title")
	}

	offset := -3
	if matched[rawContent[1]+offset] != ' ' {
		offset = -2
	}
	imageContent := matched[rawContent[0]+2 : rawContent[1]+offset]

	return string(imageContent), nil
}
