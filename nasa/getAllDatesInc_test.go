package nasa

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestGetAllDatesInc(t *testing.T) {
  tables := []struct{
    Name string
    InputStart APIDate
    InputEnd APIDate
    Result []APIDate
  }{
    {
      Name: "All Valid stuff",
      InputStart: APIDate{
        Year: 2020,
        Month: 10,
        Day: 1,
      },
      InputEnd: APIDate{
        Year: 2020,
        Month: 10,
        Day: 2,
      },
      Result: []APIDate{
        {
          Year: 2020,
          Month: 10,
          Day: 1,
        },
        {
          Year: 2020,
          Month: 10,
          Day: 2,
        },
      },
    },
    {
      Name: "End is before start",
      InputStart: APIDate{
        Year: 2020,
        Month: 10,
        Day: 1,
      },
      InputEnd: APIDate{
        Year: 2020,
        Month: 9,
        Day: 1,
      },
      Result: []APIDate{},
    },
  }

  for _, table := range tables {
    inStart := table.InputStart
    inEnd := table.InputEnd
    result := table.Result

    t.Run(table.Name, func (t *testing.T) {
      output := getAllDatesInc(inStart, inEnd)

      assert.ElementsMatch(t, result, output)
    })
  }
}
