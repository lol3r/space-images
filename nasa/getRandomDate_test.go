package nasa

import (
	"testing"
)

func TestGetRandomDate(t *testing.T) {
	tables := []struct {
		Name      string
		StartDate APIDate
		EndDate   APIDate
	}{
		{
			Name: "Valid",
			StartDate: APIDate{
				Year:  2010,
				Month: 10,
				Day:   23,
			},
			EndDate: APIDate{
				Year:  2015,
				Month: 4,
				Day:   3,
			},
		},
	}

	for _, table := range tables {
		start := table.StartDate
		end := table.EndDate

		t.Run(table.Name, func(t *testing.T) {
			out := GetRandomDate(start, end)

			if out.Year < start.Year || out.Year > end.Year {
				t.Fatalf("Year out of range[%d - %d]: %d", start.Year, end.Year, out.Year)
			}
		})
	}
}
