# Building the binary of the App

FROM golang:1.15 AS build

WORKDIR /go/src/space-images

COPY go.mod .
COPY go.sum .

RUN go mod download

ENV CGO_ENABLED=0 \
    GOOS=linux \
    GOARCH=amd64

COPY . .

RUN go build -a -installsuffix cgo -o app .


# Moving the binary to the 'final Image' to make it smaller
FROM alpine:latest

WORKDIR /app

COPY --from=build /go/src/space-images/app .

EXPOSE 80

CMD ["./app"]
