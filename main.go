package main

import (
  "space-images/api"

  "github.com/sirupsen/logrus"
)

func main() {
  logrus.Infof("Starting...")

  api := api.Setup()

  err := api.Start(80)
  if err != nil {
    logrus.Errorf("Starting API: %v", err)
  }
}
