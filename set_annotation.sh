#!/bin/bash

curl -XPOST https://grafana.lol3r.net/api/annotations \
  -H "Content-Type: application/json" \
  -H "Authorization: Bearer ${GRAFANA_API_KEY}" \
  --data @- << EOF
  {
    "text": "Deployed service 'space-images-backend'\n\n
      <a href=\"https://gitlab.com/lol3r/space-images/-/commit/${CI_COMMIT_SHA}\" target=\"_blank\">Git (${CI_COMMIT_SHORT_SHA})</a>",
    "tags": [
      "deployment",
      "tier:frontend"
    ]
  }
EOF
