package api

import (
	"space-images/nasa"
	"time"

	"github.com/sirupsen/logrus"
)

func (a *internalAPI) imageLoaderBackground(waitTime time.Duration) {
	startDate := nasa.APIDate{
		Year:  1995,
		Month: 6,
		Day:   16,
	}

	for {
		logrus.Infof("Loading new Images...")

		today := time.Now()
		endDate := nasa.APIDate{
			Year:  today.Year(),
			Month: int(today.Month()),
			Day:   today.Day(),
		}

		images := nasa.LoadImages(startDate, endDate, 10)

		a.imageMutex.Lock()
		a.Images = images
		a.imageMutex.Unlock()

		logrus.Infof("Done loading new Images")

		time.Sleep(waitTime)
	}
}
