package api

import (
	"space-images/nasa"

	"github.com/gofiber/fiber/v2"
)

// Setup is used to obtain a valid api struct that is ready to start
func Setup() API {
	tmp := &internalAPI{
		Images: nasa.GetEmptyImageBundle(),
	}

	app := fiber.New()

	app.Get("/image/random", tmp.handleGetRandom)
	app.Get("/image/day", tmp.handleSpecificDay)

	// Readiness probe for kubernetes, etc.
	app.Get("/ready", tmp.handleReady)

	tmp.app = app

	return tmp
}
