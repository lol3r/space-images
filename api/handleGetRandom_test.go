package api

import (
	"errors"
	"io/ioutil"
	"net/http"
	"space-images/nasa"
	"space-images/nasa/mocks"
	"testing"

	"github.com/gofiber/fiber/v2"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
)

func TestHandleGetRandom(t *testing.T) {
	tables := []struct {
		Name         string
		InputAPI     *internalAPI
		ResultBody   string
		ResultStatus int
	}{
		{
			Name: "Valid",
			InputAPI: &internalAPI{
				Images: &mocks.MockImageBundle{
					Mock: mock.Mock{
						ExpectedCalls: []*mock.Call{
							{
								Method: "GetRandomImage",
								ReturnArguments: mock.Arguments{
									&nasa.Image{
										Title: "testTitle",
									},
									nil,
								},
							},
						},
					},
				},
			},
			ResultBody:   `{"title":"testTitle","imageURL":"","pageURL":"","date":{"year":0,"month":0,"day":0}}`,
			ResultStatus: 200,
		},
		{
			Name: "NASA returns error",
			InputAPI: &internalAPI{
				Images: &mocks.MockImageBundle{
					Mock: mock.Mock{
						ExpectedCalls: []*mock.Call{
							{
								Method: "GetRandomImage",
								ReturnArguments: mock.Arguments{
									&nasa.Image{},
									errors.New("testError"),
								},
							},
						},
					},
				},
			},
			ResultBody:   `Could not find Image`,
			ResultStatus: 500,
		},
	}

	for _, table := range tables {
		inAPI := table.InputAPI
		resBody := table.ResultBody
		resStatus := table.ResultStatus

		t.Run(table.Name, func(t *testing.T) {
			app := fiber.New()
			app.Get("/image/random", inAPI.handleGetRandom)

			req, err := http.NewRequest("GET", "/image/random", nil)
			if err != nil {
				t.Fatal(err)
			}

			resp, err := app.Test(req)
			if err != nil {
				t.Fatal(err)
			}

			outStatus := resp.StatusCode
			assert.Equal(t, resStatus, outStatus)

			outBody, err := ioutil.ReadAll(resp.Body)
			if err != nil {
				t.Fatal(err)
			}
			assert.Equal(t, resBody, string(outBody))
		})
	}
}
