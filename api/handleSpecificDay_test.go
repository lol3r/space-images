package api

import (
	"errors"
	"io/ioutil"
	"net/http"
	"testing"

	"space-images/nasa"
	"space-images/nasa/mocks"

	"github.com/gofiber/fiber/v2"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
)

func TestHandleSpecifcDay(t *testing.T) {
	tables := []struct {
		Name         string
		InputAPI     *internalAPI
		QueryString  string
		ResultBody   string
		ResultStatus int
	}{
		{
			Name: "Valid Request",
			InputAPI: &internalAPI{
				Images: &mocks.MockImageBundle{
					Mock: mock.Mock{
						ExpectedCalls: []*mock.Call{
							{
								Method: "GetSpecificDay",
								Arguments: mock.Arguments{
									nasa.APIDate{
										Year:  2020,
										Month: 10,
										Day:   3,
									},
								},
								ReturnArguments: mock.Arguments{
									&nasa.Image{
										Title: "testTitle",
									},
									nil,
								},
							},
						},
					},
				},
			},
			QueryString:  "year=2020&month=10&day=3",
			ResultBody:   `{"title":"testTitle","imageURL":"","pageURL":"","date":{"year":0,"month":0,"day":0}}`,
			ResultStatus: 200,
		},
		{
			Name: "No Day found",
			InputAPI: &internalAPI{
				Images: &mocks.MockImageBundle{
					Mock: mock.Mock{
						ExpectedCalls: []*mock.Call{
							{
								Method: "GetSpecificDay",
								Arguments: mock.Arguments{
									nasa.APIDate{
										Year:  2020,
										Month: 10,
										Day:   3,
									},
								},
								ReturnArguments: mock.Arguments{
									&nasa.Image{},
									errors.New("testError"),
								},
							},
						},
					},
				},
			},
			QueryString:  "year=2020&month=10&day=3",
			ResultBody:   `testError`,
			ResultStatus: 500,
		},
		{
			Name: "Part of date missing, year",
			InputAPI: &internalAPI{
				Images: &mocks.MockImageBundle{
					Mock: mock.Mock{
						ExpectedCalls: []*mock.Call{},
					},
				},
			},
			QueryString:  "month=4&day=3",
			ResultBody:   `Incomplete Date`,
			ResultStatus: 500,
		},
		{
			Name: "Part of date missing, month",
			InputAPI: &internalAPI{
				Images: &mocks.MockImageBundle{
					Mock: mock.Mock{
						ExpectedCalls: []*mock.Call{},
					},
				},
			},
			QueryString:  "year=2020&day=3",
			ResultBody:   `Incomplete Date`,
			ResultStatus: 500,
		},
		{
			Name: "Part of date missing, day",
			InputAPI: &internalAPI{
				Images: &mocks.MockImageBundle{
					Mock: mock.Mock{
						ExpectedCalls: []*mock.Call{},
					},
				},
			},
			QueryString:  "year=2020&month=3",
			ResultBody:   `Incomplete Date`,
			ResultStatus: 500,
		},
	}

	for _, table := range tables {
		inAPI := table.InputAPI
		query := table.QueryString
		resBody := table.ResultBody
		resStatus := table.ResultStatus

		t.Run(table.Name, func(t *testing.T) {
			app := fiber.New()
			app.Get("/image/day", inAPI.handleSpecificDay)

			req, err := http.NewRequest("GET", "/image/day?"+query, nil)
			if err != nil {
				t.Fatal(err)
			}

			resp, err := app.Test(req)
			if err != nil {
				t.Fatal(err)
			}

			outStatus := resp.StatusCode
			assert.Equal(t, resStatus, outStatus)

			outBody, err := ioutil.ReadAll(resp.Body)
			if err != nil {
				t.Fatal(err)
			}
			assert.Equal(t, resBody, string(outBody))
		})
	}
}
