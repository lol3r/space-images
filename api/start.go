package api

import (
	"strconv"
	"strings"
	"time"
)


func (a *internalAPI) Start(port int) error {
  var address strings.Builder
  address.WriteString(":")
  address.WriteString(strconv.Itoa(port))

  go a.imageLoaderBackground(6 * time.Hour)

  return a.app.Listen(address.String())
}
