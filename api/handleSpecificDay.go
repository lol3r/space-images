package api

import (
	"errors"
	"space-images/nasa"

	"github.com/gofiber/fiber/v2"
)

func (a *internalAPI) handleSpecificDay(ctx *fiber.Ctx) error {
	var date nasa.APIDate
	if err := ctx.QueryParser(&date); err != nil {
		return err
	}
	if date.Year == 0 || date.Month == 0 || date.Day == 0 {
		return errors.New("Incomplete Date")
	}

	image, err := a.Images.GetSpecificDay(date)
	if err != nil {
		return err
	}

	return ctx.JSON(image)
}
