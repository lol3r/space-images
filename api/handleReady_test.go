package api

import (
	"net/http"
	"testing"

	"space-images/nasa/mocks"

	"github.com/gofiber/fiber/v2"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
)

func TestHandleReady(t *testing.T) {
	tables := []struct {
		Name         string
		InputAPI     *internalAPI
		ResultStatus int
	}{
		{
			Name: "Empty Content",
			InputAPI: &internalAPI{
				Images: &mocks.MockImageBundle{
					Mock: mock.Mock{
						ExpectedCalls: []*mock.Call{
							{
								Method: "HasContent",
								ReturnArguments: mock.Arguments{
									false,
								},
							},
						},
					},
				},
			},
			ResultStatus: 500,
		},
		{
			Name: "At least one Image",
			InputAPI: &internalAPI{
				Images: &mocks.MockImageBundle{
					Mock: mock.Mock{
						ExpectedCalls: []*mock.Call{
							{
								Method: "HasContent",
								ReturnArguments: mock.Arguments{
									true,
								},
							},
						},
					},
				},
			},
			ResultStatus: 200,
		},
	}

	for _, table := range tables {
		inAPI := table.InputAPI
		resStatus := table.ResultStatus

		t.Run(table.Name, func(t *testing.T) {
			app := fiber.New()
			app.Get("/ready", inAPI.handleReady)

			req, err := http.NewRequest("GET", "/ready", nil)
			if err != nil {
				t.Fatal(err)
			}

			resp, err := app.Test(req)
			if err != nil {
				t.Fatal(err)
			}

			assert.Equal(t, resStatus, resp.StatusCode)
		})
	}
}
