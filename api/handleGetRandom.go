package api

import (
	"errors"

	"github.com/gofiber/fiber/v2"
	"github.com/sirupsen/logrus"
)

func (a *internalAPI) handleGetRandom(ctx *fiber.Ctx) error {
	a.imageMutex.RLock()
	defer a.imageMutex.RUnlock()

	image, err := a.Images.GetRandomImage()

	if err != nil {
		logrus.Errorf("Getting Random Image: %s", err)
		return errors.New("Could not find Image")
	}

	return ctx.JSON(image)
}
