package api

import (
	"space-images/nasa"
	"sync"

	"github.com/gofiber/fiber/v2"
)

type internalAPI struct {
	app *fiber.App

	imageMutex sync.RWMutex
	Images     nasa.ImageBundle
}

// API is a simple interface that is used to hide the actual implementation
type API interface {
	Start(port int) error
}
