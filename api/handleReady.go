package api

import "github.com/gofiber/fiber/v2"

func (a *internalAPI) handleReady(ctx *fiber.Ctx) error {
	a.imageMutex.RLock()
	defer a.imageMutex.RUnlock()

	if !a.Images.HasContent() {
		return ctx.SendStatus(500)
	}

	return ctx.SendStatus(200)
}
