# Space-Images

## Endpoints
* /image/random: Loads a random Image from its image storage
* /image/day: Loads the Image of a specific date
* /ready: Returns 200 once the service is ready to accept requests (used for readiness probes)
